package com.example.nfcwrite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.rt.printerlibrary.bean.SerialPortConfigBean;
import com.rt.printerlibrary.connect.PrinterInterface;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.factory.connect.PIFactory;
import com.rt.printerlibrary.factory.connect.SerailPortFactory;
import com.rt.printerlibrary.factory.printer.PrinterFactory;
import com.rt.printerlibrary.factory.printer.ThermalPrinterFactory;
import com.rt.printerlibrary.observer.PrinterObserver;
import com.rt.printerlibrary.observer.PrinterObserverManager;
import com.rt.printerlibrary.printer.RTPrinter;
import com.rt.printerlibrary.utils.PrinterPowerUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements PrinterObserver, AdapterView.OnItemSelectedListener {
    EditText etUsername;
    Button btnWrite;
    Button btnread;
    Button btnreload;
    ProgressBar progressbar;
    Spinner spinner;

    ApiInterface apiInterface;

    List<String> listOfCardNumber = Arrays.asList("ganesh", "daju", "mero", "kaam", "sakkyo", "api", "matra", "baki");


    //new printer v2
    @BaseEnum.ConnectType
    public static int checkedConType = BaseEnum.CON_COM;
    public static RTPrinter rtPrinter = null;
    public static PrinterFactory printerFactory;
    public static Object configObj;
    public static PrinterPowerUtil printerPowerUtil;


    boolean mWriteMode = false;
    private PendingIntent mNfcPendingIntent;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    String name = null;
    Integer amount = null;
    String newNFCID = "";

    byte[] keytosetup = new byte[] { (byte) 0x3c, (byte) 0x55, (byte) 0x28,
            (byte) 0x12, (byte) 0x5c, (byte) 0x61, (byte) 0x00,
            (byte) 0x5C, (byte) 0x71, (byte) 0x00 };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialise();
        initPrinter();
        //spiinerere

//        spinner.setOnItemSelectedListener(this);
//        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,listOfCardNumber);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(aa);
        //spinner ends

        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<List<String>> call = apiInterface.getCardList();
        Log.e("call", call.request().toString() + "");
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {

                progressbar.setVisibility(View.GONE);

                Log.e("respose", response.code() + "");
                Log.e("resposeasd", response.isSuccessful() + "");

                Log.e("status", response.code() + "");
                if (response.code() == 200) {

                    Log.e("jaj", "1234");
                    listOfCardNumber = response.body();
//                    System.out.println("=== " + response.body() + listOfCardNumber.get(1));
//                    spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) getApplicationContext());
//                    spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) getApplicationContext());
                    spinner.setOnItemSelectedListener(MainActivity.this);

                    ArrayAdapter aa = new ArrayAdapter(MainActivity.this,android.R.layout.simple_spinner_item,listOfCardNumber);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(aa);


                    Toast.makeText(getApplicationContext(),"Success ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Log.e("Here", t.getMessage());
            }
        });




        NfcManager mNfcManager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        mNfcAdapter = mNfcManager.getDefaultAdapter();
        Log.e("isNull", mNfcAdapter + "");

        Intent intent = new Intent(this, getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mPendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mNfcPendingIntent = PendingIntent.getActivity(MainActivity.this, 0,
                new Intent(MainActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        btnread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent readintent = new Intent(MainActivity.this,Readactivity.class);
                startActivity(readintent);
            }
        });
        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = etUsername.getText().toString();
              
                updateCardStatusApiCall(etUsername.getText().toString());
                disableTagWriteMode();
                activateNfc();
            }
        });


        btnreload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }


    private void updateCardStatusApiCall(String cardid){
        Toast.makeText(this,""+cardid , Toast.LENGTH_SHORT).show();
        apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.updateCardStatus(cardid);
        Log.e("call", call.request().toString() + "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressbar.setVisibility(View.GONE);

                if (response.code() == 200) {

                    Toast.makeText(getApplicationContext(),"Success UPDATED/REGISTERED ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"update vayena NOOOOOOO ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private NfcAdapter activateNfc() {
        NfcAdapter nfcadapter=mNfcAdapter;
        if (nfcadapter != null) {
            try {
                IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
                IntentFilter[] mWriteTagFilters = new IntentFilter[]{tagDetected};
                //mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
                nfcadapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);
            }catch (IllegalStateException e) {
                Log.e("ActionIntent", "Error enable NFC foreground dispatch" +e.getMessage());
            }
            return nfcadapter;

        }
        return null;
    }

































    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            processIntentWrite(intent);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void processIntentWrite(Intent intent) throws RemoteException {
        System.out.println("write ma chiryo");
        mWriteMode = true;
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
         /*   try {
                newNFCID = Encryption.enc(name);
                System.out.println("The card id is " + name);
                System.out.println("key " + newNFCID);

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("info is " + newNFCID.length());*/

            byte[] bytecurrent = name.getBytes();
            //System.out.println("info is byte lenght " +bytecurrent);
            System.out.println("info is byte lenght " +bytecurrent.length);
            System.out.println(Arrays.toString(bytecurrent));
            NdefRecord record = NdefRecord.createMime("A", bytecurrent);
            NdefMessage message = new NdefMessage(new NdefRecord[]{record});

            if (writeTag(message, detectedTag)) {
                Toast.makeText(this, "Write Successful", Toast.LENGTH_LONG).show();
                Intent i = getIntent();
                finish();
                startActivity(i);
            } else {

                Toast.makeText(this, "not written 1", Toast.LENGTH_SHORT).show();


            }
        } else {
            Toast.makeText(this, "not discovered 1", Toast.LENGTH_SHORT).show();

        }
    }
    public boolean writeTag(NdefMessage message, Tag tag) throws RemoteException {
        int size = message.toByteArray().length;

        System.out.println("trying to write");

        entrydata(message,tag, name.getBytes(Charset.forName("UTF8")),name);


return true;


        }


    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }

    private void initialise() {
        etUsername = findViewById(R.id.etUsername);
        btnWrite = findViewById(R.id.btnWrite);
        btnread = findViewById(R.id.btnread);
        btnreload = findViewById(R.id.btnreload);
        spinner = findViewById(R.id.spinner);
        progressbar = findViewById(R.id.progressbar);
    }

    public String toHex(String arg) throws UnsupportedEncodingException {
        return String.format("%x", new BigInteger(1, arg.getBytes("UTF-8")));
    }









    public byte my_custom_key[] = {
            (byte) 0xd3, (byte) 0xf7, (byte) 0xda, (byte) 0xf7, (byte) 0xf3, (byte) 0x17
            , (byte) 0x7f, (byte) 0x07, (byte) 0x88, (byte) 0x40,
            (byte) 0xd3, (byte) 0xf2, (byte) 0xd3, (byte) 0xf6, (byte) 0xd3, (byte) 0xf7
    };
    public int CHUNK_SIZE=16;
    public byte customKeyA[] = {(byte) 0xd3, (byte) 0xf7, (byte) 0xda, (byte) 0xf7, (byte) 0xf3, (byte) 0x17};
    public byte customKeyB[] = {(byte) 0xd3, (byte) 0xf2, (byte) 0xd3, (byte) 0xf6, (byte) 0xd3, (byte) 0xf7};

    public Integer requiredkeylength = 3;

    public String entrydata(NdefMessage message, Tag tag,  byte[] messagebytevalue,String messageactual) {
        MifareClassic mfc = MifareClassic.get(tag);
         System.out.println(Arrays.toString( messagebytevalue));
        System.out.println("tag hear");
        if(tag==null){
            System.out.println("null tag hear");
        }
      /*  int requiredsectortowritedate = 1;

        if (messagebytevalue.length > requiredkeylength) {
            if (messagebytevalue.length % requiredkeylength == 0) {
                requiredsectortowritedate = messagebytevalue.length / requiredkeylength;
            } else {
                requiredsectortowritedate = (messagebytevalue.length / requiredkeylength) + 1;
            }
        }*/


        //split message
        System.out.println("765");
        byte[][] splitMessage = splitChunks(messagebytevalue);
        System.out.println("splitmesage");
        int requiredsectortowritedate = 0;
        if (splitMessage.length > requiredkeylength) {
            if (splitMessage.length % requiredkeylength == 0) {
                requiredsectortowritedate = messagebytevalue.length / requiredkeylength;
            } else {
                requiredsectortowritedate = (messagebytevalue.length / requiredkeylength) + 1;
            }
        }

       // int currentdetailcount = 1;
        System.out.println("currentdetailcount");
        for (int i = 0; i <= requiredsectortowritedate; i++) {
// begin to write on block

// cant write on value 0 so need to search from 1
            Blocakdetail blockdetail = setblockDetail(i + 1);
            System.out.println("blockdetail");
            if (blockdetail != null) {
// filter 3 each
                int k = (3 * i);
                int toreah = k + 3;
                int writeonblock = 0;
                for (int p = k; p <= toreah; p++) {

                    try {
                        // trying to write on sector trail
                        mfc.connect();
                        System.out.println("on sector"+blockdetail.getSector());
                        if (mfc.authenticateSectorWithKeyA(blockdetail.getSector(), MifareClassic.KEY_DEFAULT) &&
                                mfc.authenticateSectorWithKeyB(blockdetail.getSector(), MifareClassic.KEY_DEFAULT)) {
                            byte[] datatoadd = null;
                            byte[] result = new byte[100];
                            try {

                                datatoadd = splitMessage[p];
                                if(datatoadd.length<16){
                                    System.err.println("small"+datatoadd.length);
                                    datatoadd = new BigInteger(datatoadd).setBit(16).toByteArray();
                                                                                    

                                }
                                System.out.println(datatoadd[p]);
                                System.out.println(datatoadd);
                               
                            } catch (Exception e) {
                                // if array is not avilable just add 00 in byte info
                                System.out.println(e.getMessage());
                                datatoadd = new byte[]{00, 00, 00, 00, 00, 00, 00, 00,00, 00, 00, 00, 00, 00, 00, 00};
                            }
                            if (writeonblock == 0) {
                                System.out.println("writing on block 0 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock0(), datatoadd);
                                // print the data in the file as we need the file to print

                            } else if (writeonblock == 1) {
                                System.out.println("writing on block 1 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock1(), datatoadd);
                            } else if (writeonblock == 2) {
                                System.out.println("writing on block 2 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock2(), datatoadd);
                            } else if (writeonblock == 3) {
                                System.out.println("writing on sector trail of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getSectorTrail(), my_custom_key);
                                Toast.makeText(getApplicationContext(),"CHINGADA MADRE WRITE SUCCESSFULLLLLLLLLLLLL",Toast.LENGTH_SHORT).show();

//                                PrintClass printclass=new PrintClass(this,0,0,1,1,messageactual,"card");
//                                printclass.Print("CardInitilize");
                            }
                            writeonblock++;
                        }
                        mfc.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // add block key
            }
            System.out.println("block detail null");
// block detail is null


        }


        return null;
    }


    public byte[][] splitChunks(byte[] source)
    {
        System.out.println("convert byte");
        byte[][] ret = new byte[(int)Math.ceil(source.length / (double)CHUNK_SIZE)][];
        int start = 0;
        for(int i = 0; i < ret.length; i++) {
            System.out.println(i);
            if(start + CHUNK_SIZE > source.length) {
                ret[i] = new byte[source.length-start];
                System.arraycopy(source, start, ret[i], 00, source.length - start);
            }
            else {
                ret[i] = new byte[CHUNK_SIZE];
                System.arraycopy(source, start, ret[i], 00, CHUNK_SIZE);
            }
            System.out.println(ret[i].length);
                    System.out.println(Arrays.toString( ret[i]));

            start += CHUNK_SIZE ;
        }
        System.out.println(Arrays.toString( ret));
        return ret;
    }

    public Blocakdetail setblockDetail(int datanumber){
        System.out.println(datanumber);
        Blocakdetail blockdetail=new Blocakdetail();
        if(datanumber==1){
            blockdetail.setSector(1);
            blockdetail.setBlock0(4);
            blockdetail.setBlock1(5);
            blockdetail.setBlock2(6);
            blockdetail.setSectorTrail(7);
            return blockdetail;
        }

        return null;

    }

    public void initPrinter(){

        System.out.println("printer init vayo");

        //Initialize Thermalprinter
        BaseApplication baseApplication = new BaseApplication();
        baseApplication.onCreate();
        baseApplication.setCurrentCmdType(BaseEnum.CMD_ESC);
        BaseApplication.instance.setCurrentCmdType(BaseEnum.CMD_ESC);
        printerFactory = new ThermalPrinterFactory();
        rtPrinter = printerFactory.create();

        PrinterObserverManager.getInstance().add(this);  //  add connection state listening                                            添加连接状态监听

        configObj = new SerialPortConfigBean().getDefaultConfig();
        printerPowerUtil = new PrinterPowerUtil(this);

        doConnect();
    }

    @Override
    public void printerObserverCallback(PrinterInterface printerInterface, int state) {
        System.out.println("yeta pugexa main ko calllback");

        switch (state) {
            case CommonEnum.CONNECT_STATE_SUCCESS:
//                        showToast(printerInterface.getConfigObject().toString() + getString(R.string._main_connected));
//                        tv_device_selected.setText(printerInterface.getConfigObject().toString());
                rtPrinter.setPrinterInterface(printerInterface);
                BaseApplication.getInstance().setRtPrinter(rtPrinter);
//                        setPrintEnable(true);
                System.out.println("CONNECT_STATE_SUCCESS MAIN bhako awastha");
                break;
            case CommonEnum.CONNECT_STATE_INTERRUPTED:
                if (printerInterface != null && printerInterface.getConfigObject() != null) {
//                            showToast(printerInterface.getConfigObject().toString() + getString(R.string._main_disconnect));
                    System.out.println("CONNECT_STATE_intrtupt MAIN bhako awastha");
                } else {
                    System.out.println("CONNECT_STATE_interupt MAIN else bhako awastha");
//                            showToast(getString(R.string._main_disconnect));
                }
//                        tv_device_selected.setText(R.string.please_connect);
                BaseApplication.getInstance().setRtPrinter(null);
                break;
            default:
                break;
        }
//            }

    }

    @Override
    public void printerReadMsgCallback(PrinterInterface printerInterface, byte[] bytes) {

    }


    private void doConnect() {
        System.out.println("do connect");

        switch (checkedConType) {
            case BaseEnum.CON_COM:
                System.out.println("do connnnect case1`1");
                connectSerialPort((SerialPortConfigBean) configObj);
                printerPowerUtil.setPrinterPower(true);//turn printer power on.
                break;
            default:
                break;
        }
    }

    private void connectSerialPort(SerialPortConfigBean serialPortConfigBean) {
        PIFactory piFactory = new SerailPortFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(serialPortConfigBean);
        rtPrinter.setPrinterInterface(printerInterface);
        try {
            rtPrinter.connect(serialPortConfigBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //spinneer car d  unebrijnlfnanvnvnajkvnakjvnanumber
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        etUsername.setText(listOfCardNumber.get(i));



    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}