package com.example.nfcwrite;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.BatteryManager;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.rt.printerlibrary.bean.SerialPortConfigBean;
import com.rt.printerlibrary.cmd.Cmd;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.connect.PrinterInterface;
import com.rt.printerlibrary.factory.cmd.CmdFactory;
import com.rt.printerlibrary.factory.connect.PIFactory;
import com.rt.printerlibrary.factory.connect.SerailPortFactory;
import com.rt.printerlibrary.printer.ThermalPrinter;
import com.rt.printerlibrary.setting.TextSetting;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import static com.example.nfcwrite.MainActivity.checkedConType;
import static com.example.nfcwrite.MainActivity.configObj;
import static com.example.nfcwrite.MainActivity.printerPowerUtil;
import static com.example.nfcwrite.MainActivity.rtPrinter;

class PrintClass {
    TextView title_tv;
    private final int NOPAPER = 3;
    private final int LOWBATTERY = 4;
    private final int OVERHEAT = 12;
    private String picturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/test.bmp";
    //    MyHandler handler;
    private String Result;
    private boolean LowBattery = false;
    ThermalPrinter usbThermalPrinter;
    String currentDate = "", currentTime = "";
    int totalprice = 0, totalPassenger = 0, id = 0, totalNew = 0;
    String thankyou, cardid = null;
    String currentMoney = "";
    String rechargeAmount = "";
    String parkingname = "", vehicletype = "", transactid = "", vehiclenumber = " ";
    String paymentMode = "";
    Double chargedamount = 0.0;
    Context c;
    Bitmap qrCode = null;
    byte b1[] = new byte[256];


    private TextSetting textSetting;

    public PrintClass(Context context, int totalprice, int totalPassenger, int i, int totalNew, String cardid, String paymentMode) {
        this.c = context;
        this.usbThermalPrinter = new ThermalPrinter();
        this.currentTime = currentTime;
        this.currentDate = currentDate;
        this.totalprice = totalprice;
        this.totalPassenger = totalPassenger;
        this.totalNew = totalNew;
        this.id = i;
        this.cardid = cardid;
        this.paymentMode = paymentMode;
    }

    public PrintClass(Context applicationContext, String currentMoney, String rechargeAmount, int totalnew, String cardid) {
        this.c = applicationContext;
        this.usbThermalPrinter = new ThermalPrinter();
        this.currentMoney = currentMoney;
        this.rechargeAmount = rechargeAmount;
        this.totalprice = totalnew;
        this.cardid = cardid;
    }

    public PrintClass(Context applicationContext, String id, String parkingname, Double charges, String vehicletype, String vehiclenumber) {
        this.c = applicationContext;
        this.usbThermalPrinter = new ThermalPrinter();
        this.transactid = id;
        this.parkingname = parkingname;
        this.vehicletype = vehicletype;
        this.vehiclenumber = vehiclenumber;
        this.chargedamount = charges;
        this.cardid = cardid;

    }


    public void Print(String task) {

        String message = initview(task);

        try {
//            initializePrinter(message);
            connectAndPrint(message);
            System.out.println("Printing message " + message);

        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported encoding exception error");
        } catch (InterruptedException ee) {
            System.out.println("interrrupt exception error");

        }


    }


    private String initview(final String task) {
        StringBuilder returnString = new StringBuilder();
        returnString.append("Digo Yatayat Services "+"\n");
        returnString.append("BUS SYSTEM \n");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);

        if (task.equals("Fare")) {
            System.out.println("fare ko print ma gayo");
            returnString.append("Bus Ticket \n");

            returnString.append("Date/time: " + str + "\n");
            returnString.append("Payment Method: " + paymentMode + "\n");
            returnString.append("Total passengers:" + " " + totalPassenger + "\n");
            returnString.append("Total amount:" + " " + totalprice + "\n");
            returnString.append("Remaining balance:" + " " + totalNew + "\n");
        } else if (task.equals("parkin")) {
            returnString.append("Your parking id: " + " " + transactid + "\n");
            returnString.append("Your vehicle type:" + " " + vehicletype + "wheeler" + "\n");
            returnString.append("Vehicle number: " + vehiclenumber + "\n");
            returnString.append("You are parked at: " + " " + parkingname + "\n");

        } else if (task.equals("parkout")) {
            returnString.append("Your parking id" + " " + transactid + "\n");
            returnString.append("Charged amount" + " " + chargedamount + "\n");
            returnString.append("Thank you for visiting" + " " + parkingname + "\n");

        }else if (task.equals("CardInitilize")){
            List<String> strings = new ArrayList<String>();
            int index = 0;
            while (index < cardid.length()) {
                strings.add(cardid.substring(index, Math.min(index + 4,cardid.length())));
                index += 4;
            }
            String finalcardid ="";
            for(String s:strings){
                finalcardid+= s+"-";
            }
           String data= finalcardid.substring(0,finalcardid.length()-1);

            returnString.append("CARDS SALES "+"\n");
            returnString.append("Thank you for buying "+"\n");
            returnString.append("eDheba Card "+"\n");
            returnString.append("for transportation & parking , "+"\n\n");
            returnString.append("Card Price: NPR 100 "+"\n");
            returnString.append("Balance: NPR 50 "+"\n");
            returnString.append("Total Purchase Price: NPR 150 "+"\n");
            returnString.append("Your Card Number is"+"\n" +  data +"\n");
            returnString.append( "You will need this number" + "\n");
            returnString.append("to contact customer support."+"\n");
            returnString.append("Please do not loose this."+"\n\n");
            returnString.append("eDheba Card Services"+"\n");
            returnString.append(  "iPay Pvt Ltd" + "\n\n\n\n");
        }
        else {

            returnString.append("Card TopUp Receipt " + "\n");
            returnString.append("Card No.: " + cardid + "\n");
//            returnString.append("Receipt No.: "+ id +"\n");
            returnString.append("Date/time:   " + str + "\n");
            returnString.append("Previous Balance:" + " " + currentMoney + "\n");
            returnString.append("Recharge Amount:" + " " + rechargeAmount + "\n");
            returnString.append("Current Balance :" + " " + totalprice + "\n");
            returnString.append("THANK YOU" + "\n");

        }




        return returnString.toString();

    }


    protected void onDestroy() {
        c.unregisterReceiver(printReceive);
        usbThermalPrinter.disConnect();
    }


    private final BroadcastReceiver printReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_NOT_CHARGING);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                if (status != BatteryManager.BATTERY_STATUS_CHARGING) {
                    if (level * 5 <= scale) {  //<20% 不打印
                        LowBattery = true;
                    } else {
                        LowBattery = false;
                    }
                } else {
                    LowBattery = false;
                }

            }

        }
    };


    /**
     * 生成条码
     *
     * @param str       条码内容
     * @param type      条码类型： AZTEC, CODABAR, CODE_39, CODE_93, CODE_128, DATA_MATRIX,
     *                  EAN_8, EAN_13, ITF, MAXICODE, PDF_417, QR_CODE, RSS_14,
     *                  RSS_EXPANDED, UPC_A, UPC_E, UPC_EAN_EXTENSION;
     * @param bmpWidth  生成位图宽,宽不能大于384，不然大于打印纸宽度
     * @param bmpHeight 生成位图高，8的倍数
     */

    public Bitmap CreateCode(String str, com.google.zxing.BarcodeFormat type, int bmpWidth, int bmpHeight) throws WriterException {
        Hashtable<EncodeHintType, String> mHashtable = new Hashtable<EncodeHintType, String>();
        mHashtable.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        str = transactid;
        // 生成二维矩阵,编码时要指定大小,不要生成了图片以后再进行缩放,以防模糊导致识别失败
        BitMatrix matrix = new MultiFormatWriter().encode(str, type, bmpWidth, bmpHeight, mHashtable);
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        // 二维矩阵转为一维像素数组（一直横着排）
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (matrix.get(x, y)) {
                    pixels[y * width + x] = 0xff000000;
                } else {
                    pixels[y * width + x] = 0xffffffff;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        // 通过像素数组生成bitmap,具体参考api
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        Log.e("here", "alksd");
        return bitmap;
    }


    public void connectAndPrint(String m1) throws UnsupportedEncodingException, InterruptedException {

        System.out.println("initiaise printer from connectANDPRINT");
        textSetting = new TextSetting();
//        doConnect();

//


        textPrint(m1);


    }


    private void doConnect() {
        System.out.println("do connect");

        switch (checkedConType) {
            case BaseEnum.CON_COM:
                System.out.println("do connnnect case1`1");
                connectSerialPort((SerialPortConfigBean) configObj);
                printerPowerUtil.setPrinterPower(true);//turn printer power on.
                break;
            default:
                break;
        }
    }

    private void doDisConnect() {
        if (rtPrinter != null && rtPrinter.getPrinterInterface() != null) {
            rtPrinter.disConnect();
        }
        printerPowerUtil.setPrinterPower(false);//turn printer power off.
//        setPrintEnable(false);
    }

    private void connectSerialPort(SerialPortConfigBean serialPortConfigBean) {
        PIFactory piFactory = new SerailPortFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(serialPortConfigBean);

        rtPrinter.setPrinterInterface(printerInterface);
        try {
            rtPrinter.connect(serialPortConfigBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void selfTestPrint() {
        System.out.println("yeta pugexa");

        switch (BaseApplication.getInstance().getCurrentCmdType()) {
            case BaseEnum.CMD_ESC:
                escSelftestPrint();
                break;
            default:
                break;
        }

    }


    private void escSelftestPrint() {
        System.out.println("yeta pugexa escSelfTestPrint");
        CmdFactory cmdFactory = new EscFactory();
        Cmd cmd = cmdFactory.create();
        cmd.append(cmd.getHeaderCmd());
        cmd.append(cmd.getSelfTestCmd());
        rtPrinter.writeMsgAsync(cmd.getAppendCmds());
    }

    private void textPrint(String m2) throws UnsupportedEncodingException {
        System.out.println("do textPrint");

        switch (BaseApplication.getInstance().getCurrentCmdType()) {
            case BaseEnum.CMD_ESC:
                escPrint(m2);
                break;
            default:
                break;
        }

    }

    private void escPrint(String m2) throws UnsupportedEncodingException {
        System.out.println("yeta pugexa escPrint ma ");

        if (rtPrinter != null) {
            System.out.println("yeta pugexa rtPrinter not null vitra" + m2);
            CmdFactory escFac = new EscFactory();
            Cmd escCmd = escFac.create();
            escCmd.append(escCmd.getHeaderCmd());//初始化, Initial

            escCmd.setChartsetName("UTF-8");// "UTF-8"

            escCmd.append(escCmd.getTextCmd(textSetting, m2));
//            escCmd.append(escCmd.getTextCmd(textSetting, "tessrt"));
//            escCmd.append(escCmd.getTextCmd(textSetting, "नेपाली टेक्स्ट  बस टिकट  प्रणाली नेपाली भाषामा तपाईको बिल येस्तो रहेको छ । रु १०"));
//            नेपाली टेक्स्ट  बस टिकट  प्रणाली नेपाली भाषामा तपाईको बिल येस्तो रहेको छ । रु १०

//            escCmd.append(escCmd.getLFCRCmd());
//            escCmd.append(escCmd.getLFCRCmd());
//            escCmd.append(escCmd.getLFCRCmd());
            escCmd.append(escCmd.getLFCRCmd());
            escCmd.append(escCmd.getLFCRCmd());
            escCmd.append(escCmd.getHeaderCmd());//初始化, Initial
            escCmd.append(escCmd.getLFCRCmd());

            rtPrinter.writeMsgAsync(escCmd.getAppendCmds());
        } else {
            System.out.println("Tesko baje null aayo");
        }
//        doDisConnect();
    }
}


