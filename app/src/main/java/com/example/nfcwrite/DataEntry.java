package com.example.nfcwrite;

import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.widget.Toast;

import java.io.IOException;

public class DataEntry {

    public byte my_custom_key[] = {
            (byte) 0xd3, (byte) 0xf7, (byte) 0xda, (byte) 0xf7, (byte) 0xf3, (byte) 0x17
            , (byte) 0x7f, (byte) 0x07, (byte) 0x88, (byte) 0x40,
            (byte) 0xd3, (byte) 0xf2, (byte) 0xd3, (byte) 0xf6, (byte) 0xd3, (byte) 0xf7
    };

    public byte customKeyA[] = {(byte) 0xd3, (byte) 0xf7, (byte) 0xda, (byte) 0xf7, (byte) 0xf3, (byte) 0x17};
    public byte customKeyB[] = {(byte) 0xd3, (byte) 0xf2, (byte) 0xd3, (byte) 0xf6, (byte) 0xd3, (byte) 0xf7};

    public Integer requiredkeylength = 3;
    public addBlockDetail addblockdetail = new addBlockDetail();

    public String entrydata(NdefMessage message, Tag tag,  byte[] messagebytevalue) {
        MifareClassic mfc = MifareClassic.get(tag);
     if(tag==null){
         System.out.println("null tag hear");
     }
      /*  int requiredsectortowritedate = 1;

        if (messagebytevalue.length > requiredkeylength) {
            if (messagebytevalue.length % requiredkeylength == 0) {
                requiredsectortowritedate = messagebytevalue.length / requiredkeylength;
            } else {
                requiredsectortowritedate = (messagebytevalue.length / requiredkeylength) + 1;
            }
        }*/


        //split message
        byte[][] splitMessage = addblockdetail.splitChunks(messagebytevalue);
        int requiredsectortowritedate = 0;
        if (splitMessage.length > requiredkeylength) {
            if (splitMessage.length % requiredkeylength == 0) {
                requiredsectortowritedate = messagebytevalue.length / requiredkeylength;
            } else {
                requiredsectortowritedate = (messagebytevalue.length / requiredkeylength) + 1;
            }
        }

        int currentdetailcount = 1;

        for (int i = 0; i <= requiredsectortowritedate; i++) {
// begin to write on block

// cant write on value 0 so need to search from 1
            Blocakdetail blockdetail = addblockdetail.setblockDetail(currentdetailcount + 1);
            if (blockdetail != null) {
// filter 3 each
                int k = (3 * i);
                int toreah = k + 3;
                int writeonblock = 0;
                for (int p = k; p <= toreah; p++) {

                    try {
                        // trying to write on sector trail
                        mfc.connect();
                        System.out.println("on sector"+blockdetail.getSector());
                        if (mfc.authenticateSectorWithKeyA(blockdetail.getSector(), MifareClassic.KEY_DEFAULT) &&
                                mfc.authenticateSectorWithKeyB(blockdetail.getSector(), MifareClassic.KEY_DEFAULT)) {
                            byte[] datatoadd = null;
                            try {
                                datatoadd = splitMessage[p];
                            } catch (Exception e) {
                                // if array is not avilable just add 00 in byte info
                                datatoadd = new byte[]{00, 00, 00, 00, 00, 00, 00, 00};
                            }
                            if (writeonblock == 0) {
                                System.out.println("writing on block 0 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock0(), datatoadd);
                            } else if (writeonblock == 1) {
                                System.out.println("writing on block 1 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock1(), datatoadd);
                            } else if (writeonblock == 2) {
                                System.out.println("writing on block 2 of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getBlock2(), datatoadd);
                            } else if (writeonblock == 3) {
                                System.out.println("writing on sector trail of sector "+blockdetail.getSector());
                                mfc.writeBlock(blockdetail.getSectorTrail(), my_custom_key);
                            }
                            writeonblock++;
                        }
                        mfc.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // add block key
            }

// block detail is null


        }


        return null;
    }

}
