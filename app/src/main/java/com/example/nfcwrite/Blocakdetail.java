package com.example.nfcwrite;

public class Blocakdetail {

    public int sector;
    public int block0;
    public int block1;
    public int block2;
    public int sectorTrail;

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getBlock0() {
        return block0;
    }

    public void setBlock0(int block0) {
        this.block0 = block0;
    }

    public int getBlock1() {
        return block1;
    }

    public void setBlock1(int block1) {
        this.block1 = block1;
    }

    public int getBlock2() {
        return block2;
    }

    public void setBlock2(int block2) {
        this.block2 = block2;
    }

    public int getSectorTrail() {
        return sectorTrail;
    }

    public void setSectorTrail(int sectorTrail) {
        this.sectorTrail = sectorTrail;
    }
}
