package com.example.nfcwrite;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Readactivity extends AppCompatActivity {
    String previousNFCID = "";
    String newNFCID = "";
    public byte customKeyA[] = {(byte) 0xd3, (byte) 0xf7, (byte) 0xda, (byte) 0xf7, (byte) 0xf3, (byte) 0x17};
    public byte customKeyB[] = {(byte) 0xd3, (byte) 0xf2, (byte) 0xd3, (byte) 0xf6, (byte) 0xd3, (byte) 0xf7};
    private PendingIntent mNfcPendingIntent;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    Button btnstartread;
    boolean mWriteMode = false;
    boolean problem = false;
    int realAmount = 0;
    String cardiD = null;
    String nfcidEncryptedData = "";
    TextView readdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readactivity);
        btnstartread = findViewById(R.id.btnstartread);
        readdata = findViewById(R.id.readdata);
        NfcManager mNfcManager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        mNfcAdapter = mNfcManager.getDefaultAdapter();
        Log.e("isNull", mNfcAdapter + "");

        Intent intent = new Intent(this, getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mPendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mNfcPendingIntent = PendingIntent.getActivity(Readactivity.this, 0,
                new Intent(Readactivity.this, Readactivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(this.getIntent().getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(this.getIntent().getAction())) {
            processIntentRead(this.getIntent());

        }
        btnstartread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                try {
//
//                    String nkeya = "key123";// 6 length only
////                        nkeya = toHex(nkeya).toUpperCase();
//                    nkeya = Integer.toHexString(Integer.parseInt(nkeya.toUpperCase()));
//                    nkeya = nkeya.substring(28, 40);
//
//                    MifareClassic mfc1 = MifareClassic.get(detectedTag);
//                    String key = nkeya;
//
//                    key = Integer.toHexString(Integer.parseInt(key.toUpperCase()));
//                    int len1 = key.length();
//                    key = key.substring(28,40);
//                    byte[] keya = new BigInteger(key, 16).toByteArray();
//
//                    //for key A or for Keb b
//
//
//                    mfc1.connect();
//                    boolean auth = mfc1.authenticateSectorWithKeyA(5, keya);
//                    if(auth)
//                    {
//
//
//                        byte[] readblock4=    mfc1.readBlock(20);
//
//                    }
//                }catch (Exception e){
//
//                }
                disableTagWriteMode();
                activateNfc();
            }
        });

    }

    private void activateNfc() {
        if (mNfcAdapter != null) {
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            IntentFilter[] mWriteTagFilters = new IntentFilter[]{tagDetected};
            mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);

            // mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
            Log.e("ActionIntent", this.getIntent().getAction() + "");
        }
    }

    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onNewIntent(Intent intent) {

        Log.e("here", "Faremainactivity");
        super.onNewIntent(intent);
      //  processIntentRead(intent);
        processIntentRead2(intent);
    }
    public void processIntentRead(Intent intent) {
        Parcelable[] msgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        try{
            if (msgs!=null && msgs.length > 0) {
                problem=false;
                NdefMessage msg = (NdefMessage) msgs[0];
                if (msg.getRecords().length > 0) {
                    NdefRecord rec = msg.getRecords()[0];
                    try {
                        nfcidEncryptedData = new String(rec.getPayload(), "UTF-8");
                        //previousBalance = currentMoney;

                        Encryption.dec(nfcidEncryptedData);
                        nfcidEncryptedData = Encryption.dec;
                        System.out.println(nfcidEncryptedData);
                        Log.e("pay1", nfcidEncryptedData);
                        JSONObject jsonObject = new JSONObject(nfcidEncryptedData);
                        System.out.println(jsonObject);
                        readdata.setText(jsonObject.toString());
                        realAmount = jsonObject.getInt("Amount");
                        cardiD = jsonObject.getString("nfcId");
                        Log.e("amount", realAmount + "");
                        System.out.println(cardiD+"khagsdiashdhiueqhfh");

                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
            else{
                Toast.makeText(this, "Tap the card again for a bit long time.", Toast.LENGTH_SHORT).show();
                Intent i = getIntent();
                finish();
                startActivity(i);
            }
        }catch (Exception e){
            Log.e("exceptionProblem",e.getMessage()+ "");
            Toast.makeText(this, "Problem in Card", Toast.LENGTH_SHORT).show();
            problem = true;
        }
    }

    public void processIntentRead2(Intent intent) {
        Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
MifareClassic mfc=MifareClassic.get(detectedTag);
        byte[] data;
try{
  mfc.connect();
 if(mfc.authenticateSectorWithKeyA(1,customKeyA)&&mfc.authenticateSectorWithKeyB(1,customKeyB)){
    data= mfc.readBlock(4);
     String str = new String(data, StandardCharsets.UTF_8);

  //   Toast.makeText(this, "data"+data.toString()+str, Toast.LENGTH_SHORT).show();
   //  System.out.println(data.toString()+str);
     readdata.setText(str);

 }
}catch (Exception e){
    Log.e("exceptionProblem",e.getMessage()+ "");
    Toast.makeText(this, "Problem in Card", Toast.LENGTH_SHORT).show();
}

    }
}