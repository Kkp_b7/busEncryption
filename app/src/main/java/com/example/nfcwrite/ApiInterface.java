package com.example.nfcwrite;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("requestCard")
    Call<List<String>> getCardList();


    @POST("requestCard/{cardid}")
    Call<ResponseBody> updateCardStatus(
            @Path("cardid") String cardid
    );


}
